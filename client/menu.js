import buttonlegal from '@/../node_modules/orbit-component-buttonlegal';
import PrivacyTipTwotone from '@vicons/material/PrivacyTipTwotone.js'
import { shallowRef } from 'vue'

export function menu (app, router, menu) {
    const IconAPI = shallowRef(PrivacyTipTwotone)
    app.use(buttonlegal, {
        router: router,
        menu: menu,
        root: 'buttonlegal',
        buttons: [
            {name: 'buttonlegal', text: 'Legal'  , component: buttonlegal , icon: IconAPI , pri: 98, path: '/buttonlegal', meta: {root: 'buttonlegal', host: location.host}},
        ]
    })
}